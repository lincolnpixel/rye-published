<?php

return array(
  'author'      => 'The Rye Agency',
  'author_url'  => 'https://rye.agency',
  'name'        => 'Rye Published',
  'description' => 'Checks to see if the current user has published an entry in a given channel',
  'version'     => '2.0.0',
  'namespace'   => 'Rye\Published'
);
